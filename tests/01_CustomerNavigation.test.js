const playwright = require('playwright');
const { test, expect } = require('@playwright/test');
//Models
const { LoginPage } = require('../Models/Login');
const { SearchPage } = require('../Models/SearchItem');
//Dataset
const user =  require('../Dataset/userLogin.json');
const userProfile =  require('../Dataset/userProfile.json');
const userSearch =  require('../Dataset/userSearch.json');

test.describe('Customer Navigation', () => {

    test.beforeEach(async ({ page }) => {
      await page.goto(user.url);
    });

    test('Login User and Add Shipping Address', async ({ page }) => {
      const loginPage = new LoginPage(page);
      await loginPage.login(user.username, user.password);
      await loginPage.navigateToShippingAddress();
      await loginPage.addShippingAddress(userProfile.addressLine1,userProfile.addressLine2,userProfile.city,userProfile.postCode);
      await loginPage.verifyAddress(userProfile.addressLine1,userProfile.addressLine2,userProfile.city,userProfile.postCode);

    });

    test('Search Item and Select Match', async ({ page }) => {
      const searchPage = new SearchPage(page);
      await searchPage.searchItem(userSearch.item);
      await searchPage.selectItem(userSearch.item, 3);
      await searchPage.verifyItem(userSearch.item);
    });


});
  
  

