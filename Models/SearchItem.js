const screenshotPath = '.\\Reports\\Screenshots\\';
const assert = require('assert');
class SearchPage {
    constructor(page) {
      this.page = page;
    }
    async searchItem(item) {
      await this.page.fill('[data-testid="SearchInput"]', item);
      await this.page.press('[data-testid="SearchInput"]', 'Enter');
      await this.page.screenshot({ path: screenshotPath+'\\SearchPage.png'});
    }
    async selectItem(itemName, itemNumber) {
      await this.page.click(':nth-match(:text("'+itemName+'"),'+itemNumber+')');
      await this.page.screenshot({ path: screenshotPath+'\\SearchedResults.png'});
    }
    async verifyItem(item) {
      assert.equal(await this.page.innerText('#result-title'), item)
      await this.page.screenshot({ path: screenshotPath+'\\SearchedItemDetails.png'});
    }                

  }
  module.exports = { SearchPage };