const screenshotPath = '.\\Reports\\Screenshots\\';
class LoginPage {
    constructor(page) {
      this.page = page;
    }
    async navigate(url) {
      await this.page.goto(url);
    }
    async login(username, password) {
      await this.page.type('[id=userName]', username);
      await this.page.type('[id=password]', password);
      await this.page.screenshot({ path: screenshotPath+'\\Customer_Profile.png'});
      await this.page.click('[id=login]');
    }
    async navigateToShippingAddress() {
        await this.page.click('[id=profile]');
        await this.page.click('[id=shippingAddress]');
    }    
    async addShippingAddress(addressLine1, addressLine2, city, postCode) {
      await this.page.click('[id=add]');
      await this.page.type('[id=firstName]', addressLine1);
      await this.page.type('[id=lastName]', addressLine2);
      await this.page.type('[id=userEmail]', city);
      await this.page.type('[id=userNumber]', postCode);
      await this.page.screenshot({ path: screenshotPath+'\\Customer_ShippingAddress.png'});
      await this.page.click('[id=submit]');
    }

    async verifyAddresss(addressLine1, addressLine2, city, postCode) {
      assert.equal(await this.page.innerText('#address-line1'), addressLine1);
      assert.equal(await this.page.innerText('#address-line2'), addressLine2);
      assert.equal(await this.page.innerText('#address-city'), city);
      assert.equal(await this.page.innerText('#address-postcode'), postCode);
      await this.page.screenshot({ path: screenshotPath+'\\Customer_AddedShippingAddress.png'});
    }

  }
  module.exports = { LoginPage };